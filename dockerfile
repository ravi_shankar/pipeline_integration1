FROM openjdk:8
EXPOSE 8080
ADD target/pipeline_integration_sample.jar pipeline_integration_sample.jar
ENTRYPOINT ["java", "-jar", "pipeline_integration_sample.jar"]